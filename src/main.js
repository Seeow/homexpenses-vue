import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import './registerServiceWorker';
import 'materialize-css';

Vue.config.productionTip = false;

Vue.prototype.$api = axios.create({
    baseURL: process.env.VUE_APP_API_URL
});
if (localStorage.getItem('access_token')) {
    Vue.prototype.$api.defaults.headers.common['Authorization'] = `Bearer ${localStorage.getItem('access_token')}`;
}

Vue.prototype.$event = new Vue();

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
