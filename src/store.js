import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const state = {
    isAuth: !!localStorage.getItem("access_token"),
    user: {},
    users: [],
    redirectRoute: 'index'
};

const getters = {
    isAuth(state) {
        return state.isAuth;
    },
    user(state) {
        return state.user;
    },
    users(state) {
        return state.users;
    },
    redirectRoute(state) {
        return state.redirectRoute;
    }
};

const mutations = {
    setIsAuth(state, value) {
        state.isAuth = value;
    },
    setUser(state, value) {
        state.user = {...value};
    },
    setUsers(state, value) {
        state.users = value;
    },
    updateUser(state, value) {
        Object.assign(state.user, value);
    },
    setRedirectRoute(state, value) {
        state.redirectRoute = value;
    }
};

const actions = {
    setRedirectRoute(store, value) {
        store.commit('setRedirectRoute', value);
    },
    setUser(store, value) {
        store.commit('setUser', value);
    },
    setUsers(store, value) {
        store.commit('setUsers', value);
    },
    updateUser(store, value) {
        store.commit('updateUser', value);
    },
    updateUsers(store) {
        Vue.prototype.$api.get('users.json').then((response) => {
            if (!response.data.error) {
                store.dispatch('setUsers', response.data.users);
            } else {
                if (response.data.message) {
                    Vue.prototype.$event.$emit('notification', response.data.message, 'error');
                }
            }
        }).catch((err) => {
            Vue.prototype.$event.$emit('notification', err.toString(), 'error');
        });
    },
    login(store, token) {
        localStorage.setItem("access_token", token);
        Vue.prototype.$api.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        store.commit('setIsAuth', true);
    },
    logout(store) {
        Vue.prototype.$api.post('users/logout.json');
        localStorage.removeItem('access_token');
        delete Vue.prototype.$api.defaults.headers.common['Authorization'];
        store.commit('setIsAuth', false);
        store.commit('setUser', {});
    }
};

export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
    strict: true
});
