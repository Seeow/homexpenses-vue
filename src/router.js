import Vue from 'vue';
import store from './store';
import Router from 'vue-router';
import Index from './views/Index.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import Profile from './views/Profile.vue';
import UsersIndex from './views/users/Index.vue';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index,
            meta: {requiresAuth: true}
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile,
            meta: {requiresAuth: true}
        },
        {
            path: '/users',
            name: 'users:index',
            component: UsersIndex,
            meta: {requiresAuth: true}
        },
        {
            path: '*',
            redirect: {name: 'index'}
        }
    ],
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            return {x: 0, y: 0};
        }
    }
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !store.getters.isAuth) {
        store.dispatch('setRedirectRoute', to.name);
        next({
            name: 'login'
        });
    } else {
        if (store.getters.isAuth) {
            store.dispatch('updateUsers');
        }

        next();
    }
});

export default router;
